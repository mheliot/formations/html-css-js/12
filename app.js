// Application
const app = Vue.createApp({

    data() {
        return {
            title: 'Gestion des articles',
            articles: []
        }
    },

    methods: {        

        addArticle(article) {
            this.articles.push(article)
        },

        closeModale() {
            this.$refs.addModale.close()
        },

        openModale() {
            this.$refs.addModale.showModal()
        },

        handleAddForm() {
            var data = new FormData(this.$refs.addForm)
            var article = {};
            data.forEach(function(value, key) {
                article[key] = value;
            });
            article.createdAt = new Date()
            this.addArticle(article)
            this.closeModale()
            this.$refs.addForm.reset()
        }
    },

    created() {
        fetch('http://my-json-server.typicode.com/mathieuheliot/courses-api/books')
            .then( response => response.json() )
            .then( data => this.articles = data )
            .catch( error => console.error(error) )
    }

})

// Composant Book
app.component('book', {
    
    props: {  
        data: {
          type: Object,
          required: false
        }
    },
    template: `
        <article class="article">
            <div class="col col--cbx"><input type="checkbox" checked="checked" /></div>
            <div class="col">{{ data.code }}</div>
            <div class="col col--main"><h2 class="title-2">{{ data.title }}</h2></div>
            <div class="col"><h3 class="title-3">{{ data.author }}</h3></div>
            <div class="col"><time :datetime="data.createdAt">{{ formatDate(data.createdAt) }}</time></div>
            <div class="col">{{ formatPrice(data.price) }}</div>
        </article>
    `,
    methods: {      

        formatDate(dateISO) {
            return new Date(dateISO).toLocaleDateString("fr-FR")
        },

        formatPrice(price) {
            return Number(price).toFixed(2) + ' €'
        }
    }
});

app.mount('#app')